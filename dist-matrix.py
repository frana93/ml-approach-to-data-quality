import pandas as pd 
from scipy.spatial.distance import pdist, squareform

df = pd.read_csv('datasets/bank-full.csv', sep=';')
print(df.head())

df1 = df[['age', 'balance', 'duration']].copy()
df1_small = df1.iloc[0:100,:]
print(df1_small.head())
# fill missing values
zero_data = df1.fillna(0)
# compute euclidean distance
distances = pdist(df1_small.values, metric = 'euclidean')
dist_matrix = squareform(distances)
print(dist_matrix)

import plotly.offline as py
import plotly.graph_objs as go
# plot Heatmap
trace = go.Heatmap(z=dist_matrix)
data = [trace]
py.plot(data, filename='labelled-heatmap')

# plot scatterplot
dist_matrix_avg = dist_matrix.mean(axis=1)
print(dist_matrix_avg.shape[0])

trace=go.Scatter(x=list(range(dist_matrix_avg.shape[0])),y=dist_matrix_avg, mode='markers')
data=[trace]
py.plot(data, filename='basic-scatter')
