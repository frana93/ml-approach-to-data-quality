"""
==============================================================
Deep Belief Network features for digit classification
==============================================================

Adapted from http://scikit-learn.org/stable/auto_examples/neural_networks/plot_rbm_logistic_classification.html#sphx-glr-auto-examples-neural-networks-plot-rbm-logistic-classification-py

Logistic regression on raw pixel values is presented for comparison. The
example shows that the features extracted by the UnsupervisedDBN help improve the
classification accuracy.
"""

from __future__ import print_function

print(__doc__)

import numpy as np

from scipy.ndimage import convolve
from sklearn import linear_model, datasets, metrics
from sklearn.cross_validation import train_test_split
from sklearn.pipeline import Pipeline
from dbn.models import UnsupervisedDBN # use "from dbn.tensorflow import SupervisedDBNClassification" for computations on TensorFlow


###############################################################################
## Load Data

# Load the data set
import numpy as np
import os

input_dir = '../output/'
directory = os.listdir(input_dir)

X = []
for f in directory:
    if f == '.DS_Store':
        pass
    else:
        X.append(np.load(input_dir + f))

X = np.asarray(X, 'float32')
maps_R = X[:,:,:,0].reshape(5,1600)

print(X.shape)
print(maps_R.shape)
print(maps_R)

input("Dataset Caricato. Press the <ENTER> key to continue.")

# Models we will use
logistic = linear_model.LogisticRegression()
dbn = UnsupervisedDBN(hidden_layers_structure=[25600, 51200],
                      batch_size=10,
                      learning_rate_rbm=0.06,
                      n_epochs_rbm=20,
                      activation_function='sigmoid')

classifier = Pipeline(steps=[('dbn', dbn),
                             ('logistic', logistic)])

###############################################################################
# Training
logistic.C = 6000.0

# Training RBM-Logistic Pipeline
classifier.fit(maps_R)

# Training Logistic regression
logistic_classifier = linear_model.LogisticRegression(C=100.0)
logistic_classifier.fit(maps_R)

###############################################################################
# Evaluation

print()

xy=range(X.shape[1]*X.shape[1])

print("Logistic regression using RBM features:\n%s\n" % (
    metrics.classification_report(classifier.predict(xy))))

print("Logistic regression using raw pixel features:\n%s\n" % (
    metrics.classification_report(logistic_classifier.predict(xy))))

###############################################################################